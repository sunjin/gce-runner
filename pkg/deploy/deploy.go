// Package p contains an HTTP Cloud Function.
package p

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"golang.org/x/oauth2/google"
	"google.golang.org/api/compute/v1"
)

const (
	projectID    = "youtubedownload-259206"
	zone         = "asia-northeast1-b"
	instanceName = "youtube-api"
)

// Deploy youtube-apiをdeployする
func Deploy(w http.ResponseWriter, r *http.Request) {

	// request
	var typeStr string
	if err := r.ParseForm(); err != nil {
		log.Printf("error parsing application/x-www-form-urlencoded: %v", err)
	} else {
		typeStr = r.FormValue("type")
	}

	// logic
	ctx := getContext()
	client, err := getClient(ctx)

	if err != nil {
		responseError(w, err.Error())
		return
	}
	service, err := getComputeService(client)

	if err != nil {
		responseError(w, err.Error())
		return
	}

	// list, err := printInstances(service)

	var result interface{}

	switch typeStr {
	case "list":
		result, err = printInstances(service)
	case "insert":
		result, err = insertInstance(service)
	case "delete":
		result, err = deleteInstance(service)

	default:
		msg := fmt.Sprintf("not found type: %s", typeStr)
		responseError(w, msg)
		return
	}

	if err != nil {
		responseError(w, err.Error())
		return
	}

	// result
	resultMap := map[string]interface{}{
		"success": true,
		"type":    typeStr,
		"list":    result,
	}

	responseMap(w, http.StatusOK, resultMap)

}

// responseMap responseです
func responseMap(w http.ResponseWriter, status int, resultMap map[string]interface{}) {
	json.NewEncoder(w).Encode(resultMap)
	w.WriteHeader(status)
}

// responseError 失敗
func responseError(w http.ResponseWriter, msg string) {
	resultMap := map[string]string{
		"error": msg,
	}
	json.NewEncoder(w).Encode(resultMap)
	w.WriteHeader(http.StatusBadRequest)
}

func getClient(ctx context.Context) (*http.Client, error) {
	return google.DefaultClient(ctx)
}

func getComputeService(client *http.Client) (*compute.Service, error) {
	return compute.New(client)
}

// getContext contextを取得する
func getContext() context.Context {
	return context.Background()
}

// printInstances 現在のinstanceを取得する
func printInstances(service *compute.Service) ([]string, error) {

	instanceService := service.Instances

	instanceList, err := instanceService.List(projectID, zone).Do()

	if err != nil {
		log.Printf("err: %s", err.Error())
		return nil, err
	}

	var resultList []string

	for _, instance := range instanceList.Items {
		log.Println(instance.Name)
		resultList = append(resultList, instance.Name)
	}

	return resultList, nil
}

// insertInstance インスタンスを作成する
func insertInstance(service *compute.Service) (string, error) {
	instanceService := service.Instances

	gceContainerDeclarationValue := "spec:\n  containers:\n    - name: youtube-api\n      image: 'sunjin110/youtubeserver:1.1.0'\n      env:\n        - name: YOUTUBE_SERVER_ADDRESS\n          value: ':80'\n        - name: ACCESS_CONTROL_ALLOW_ORIGIN\n          value: '*'\n      stdin: false\n      tty: false\n  restartPolicy: Always\n\n# This container declaration format is not public API and may change without notice. Please\n# use gcloud command-line tool or Google Cloud Console to run Containers on Google Compute Engine."
	googleLogginEnabledValue := "true"

	trueBool := true

	instance := compute.Instance{

		Kind: "compute#instance",
		Name: instanceName,
		Zone: "projects/youtubedownload-259206/zones/asia-northeast1-b",
		// MachineType: "projects/youtubedownload-259206/zones/asia-northeast1-b/machineTypes/g1-small",
		MachineType: "projects/youtubedownload-259206/zones/asia-northeast1-b/machineTypes/f1-micro",
		DisplayDevice: &compute.DisplayDevice{
			EnableDisplay: false,
		},
		Metadata: &compute.Metadata{
			Kind: "compute#metadata",
			Items: []*compute.MetadataItems{
				&compute.MetadataItems{
					Key:   "gce-container-declaration",
					Value: &gceContainerDeclarationValue,
				},
				&compute.MetadataItems{
					Key:   "google-logging-enabled",
					Value: &googleLogginEnabledValue,
				},
			},
		},
		Tags: &compute.Tags{
			Items: []string{
				"http-server",
			},
		},
		Disks: []*compute.AttachedDisk{
			&compute.AttachedDisk{
				Kind:       "compute#attachedDisk",
				Type:       "PERSISTENT",
				Boot:       true,
				Mode:       "READ_WRITE",
				AutoDelete: true,
				DeviceName: "youtube-api",
				InitializeParams: &compute.AttachedDiskInitializeParams{
					SourceImage: "projects/cos-cloud/global/images/cos-stable-78-12499-59-0",
					DiskType:    "projects/youtubedownload-259206/zones/asia-northeast1-b/diskTypes/pd-standard",
					DiskSizeGb:  10,
				},
				DiskEncryptionKey: &compute.CustomerEncryptionKey{},
			},
		},
		CanIpForward: false,
		NetworkInterfaces: []*compute.NetworkInterface{
			&compute.NetworkInterface{
				Kind:       "compute#networkInterface",
				Subnetwork: "projects/youtubedownload-259206/regions/asia-northeast1/subnetworks/default",
				AccessConfigs: []*compute.AccessConfig{
					&compute.AccessConfig{
						Kind:        "compute#accessConfig",
						Name:        "External NAT",
						Type:        "ONE_TO_ONE_NAT",
						NetworkTier: "PREMIUM",
						NatIP:       "34.84.133.238",
					},
				},
				AliasIpRanges: []*compute.AliasIpRange{},
			},
		},
		Description: "",
		Labels: map[string]string{
			"container-vm": "cos-stable-78-12499-59-0",
		},
		Scheduling: &compute.Scheduling{
			Preemptible:       false,
			OnHostMaintenance: "MIGRATE",
			AutomaticRestart:  &trueBool,
			NodeAffinities:    []*compute.SchedulingNodeAffinity{},
		},
		DeletionProtection: false,
		ReservationAffinity: &compute.ReservationAffinity{
			ConsumeReservationType: "ANY_RESERVATION",
		},
		ServiceAccounts: []*compute.ServiceAccount{
			&compute.ServiceAccount{
				Email: "44266997063-compute@developer.gserviceaccount.com",
				Scopes: []string{
					"https://www.googleapis.com/auth/devstorage.read_only",
					"https://www.googleapis.com/auth/logging.write",
					"https://www.googleapis.com/auth/monitoring.write",
					"https://www.googleapis.com/auth/servicecontrol",
					"https://www.googleapis.com/auth/service.management.readonly",
					"https://www.googleapis.com/auth/trace.append",
				},
			},
		},
	}

	_, err := instanceService.Insert(projectID, zone, &instance).Do()

	if err != nil {
		log.Println("err: ", err)
		return "", err
	}

	log.Println("Success")

	return "Success", nil
}

// deleteInstance インスタンスを削除する
func deleteInstance(service *compute.Service) (string, error) {
	instanceService := service.Instances
	_, err := instanceService.Delete(projectID, zone, instanceName).Do()

	if err != nil {
		return "", err
	}

	return "Success", nil
}
