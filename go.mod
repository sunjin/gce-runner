module gitlab.com/sunjin/gce-runner

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	golang.org/x/oauth2 v0.0.0-20191202225959-858c2ad4c8b6
	google.golang.org/api v0.14.0
)
