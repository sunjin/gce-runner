package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"golang.org/x/oauth2/google"
	compute "google.golang.org/api/compute/v1"
)

const (
	projectID    = "youtubedownload-259206"
	zone         = "asia-northeast1-b"
	instanceName = "youtube-api"
)

func main() {
	log.Println("=== gce runner ===")

	ctx := getContext()

	client, _ := getClient(ctx)

	service, err := getComputeService(client)

	log.Println("===>> instance list")
	// printInstances(service)
	deleteInstance(service)

	if err != nil {
		log.Printf("err: %s", err.Error())
		os.Exit(1)
	}

	// log.Println("===>> insert instance !!!")
	// insertInstance(service)

}

// printInstances 現在のinstanceを取得する
func printInstances(service *compute.Service) {

	instanceService := service.Instances

	instanceList, err := instanceService.List(projectID, zone).Do()

	if err != nil {
		log.Printf("err: %s", err.Error())
		os.Exit(1)
	}

	for _, instance := range instanceList.Items {
		log.Println(instance.Name)
	}

}

// insertInstance インスタンスを作成する
func insertInstance(service *compute.Service) {
	instanceService := service.Instances

	// POST https://www.googleapis.com/compute/v1/projects/youtubedownload-259206/zones/asia-northeast1-b/instances
	// {
	//   "kind": "compute#instance",
	//   "name": "youtube-api",
	//   "zone": "projects/youtubedownload-259206/zones/asia-northeast1-b",
	//   "machineType": "projects/youtubedownload-259206/zones/asia-northeast1-b/machineTypes/g1-small",
	//   "displayDevice": {
	//     "enableDisplay": false
	//   },
	//   "metadata": {
	//     "kind": "compute#metadata",
	//     "items": [
	//       {
	//         "key": "gce-container-declaration",
	//         "value": "spec:\n  containers:\n    - name: youtube-api\n      image: 'sunjin110/youtubeserver:1.1.0'\n      env:\n        - name: YOUTUBE_SERVER_ADDRESS\n          value: ':80'\n        - name: ACCESS_CONTROL_ALLOW_ORIGIN\n          value: '*'\n      stdin: false\n      tty: false\n  restartPolicy: Always\n\n# This container declaration format is not public API and may change without notice. Please\n# use gcloud command-line tool or Google Cloud Console to run Containers on Google Compute Engine."
	//       },
	//       {
	//         "key": "google-logging-enabled",
	//         "value": "true"
	//       }
	//     ]
	//   },
	//   "tags": {
	//     "items": [
	//       "http-server"
	//     ]
	//   },
	//   "disks": [
	//     {
	//       "kind": "compute#attachedDisk",
	//       "type": "PERSISTENT",
	//       "boot": true,
	//       "mode": "READ_WRITE",
	//       "autoDelete": true,
	//       "deviceName": "youtube-api",
	//       "initializeParams": {
	//         "sourceImage": "projects/cos-cloud/global/images/cos-stable-78-12499-59-0",
	//         "diskType": "projects/youtubedownload-259206/zones/asia-northeast1-b/diskTypes/pd-standard",
	//         "diskSizeGb": "10"
	//       },
	//       "diskEncryptionKey": {}
	//     }
	//   ],
	//   "canIpForward": false,
	//   "networkInterfaces": [
	//     {
	//       "kind": "compute#networkInterface",
	//       "subnetwork": "projects/youtubedownload-259206/regions/asia-northeast1/subnetworks/default",
	//       "accessConfigs": [
	//         {
	//           "kind": "compute#accessConfig",
	//           "name": "External NAT",
	//           "type": "ONE_TO_ONE_NAT",
	//           "networkTier": "PREMIUM"
	//         }
	//       ],
	//       "aliasIpRanges": []
	//     }
	//   ],
	//   "description": "",
	//   "labels": {
	//     "container-vm": "cos-stable-78-12499-59-0"
	//   },
	//   "scheduling": {
	//     "preemptible": false,
	//     "onHostMaintenance": "MIGRATE",
	//     "automaticRestart": true,
	//     "nodeAffinities": []
	//   },
	//   "deletionProtection": false,
	//   "reservationAffinity": {
	//     "consumeReservationType": "ANY_RESERVATION"
	//   },
	//   "serviceAccounts": [
	//     {
	//       "email": "44266997063-compute@developer.gserviceaccount.com",
	//       "scopes": [
	//         "https://www.googleapis.com/auth/devstorage.read_only",
	//         "https://www.googleapis.com/auth/logging.write",
	//         "https://www.googleapis.com/auth/monitoring.write",
	//         "https://www.googleapis.com/auth/servicecontrol",
	//         "https://www.googleapis.com/auth/service.management.readonly",
	//         "https://www.googleapis.com/auth/trace.append"
	//       ]
	//     }
	//   ]
	// }

	// ip指定
	// 	{ // https://cloud.google.com/compute/docs/ip-addresses/reserve-static-external-ip-address?hl=ja
	//   "name": "[INSTANCE_NAME]",
	//   "machineType": "zones/[ZONE]/machineTypes/[MACHINE_TYPE]",
	//   "networkInterfaces": [{
	//     "accessConfigs": [{
	//       "type": "ONE_TO_ONE_NAT",
	//       "name": "External NAT",
	//       "natIP": "[IP_ADDRESS]"
	//      }],
	//     "network": "global/networks/default"
	//   }],
	//   "disks": [{
	//      "autoDelete": "true",
	//      "boot": "true",
	//      "type": "PERSISTENT",
	//      "initializeParams": {
	//         "sourceImage": "projects/debian-cloud/global/images/v20150818"
	//      }
	//    }]
	//  }

	gceContainerDeclarationValue := "spec:\n  containers:\n    - name: youtube-api\n      image: 'sunjin110/youtubeserver:1.1.0'\n      env:\n        - name: YOUTUBE_SERVER_ADDRESS\n          value: ':80'\n        - name: ACCESS_CONTROL_ALLOW_ORIGIN\n          value: '*'\n      stdin: false\n      tty: false\n  restartPolicy: Always\n\n# This container declaration format is not public API and may change without notice. Please\n# use gcloud command-line tool or Google Cloud Console to run Containers on Google Compute Engine."
	googleLogginEnabledValue := "true"

	trueBool := true

	instance := compute.Instance{

		Kind: "compute#instance",
		Name: instanceName,
		Zone: "projects/youtubedownload-259206/zones/asia-northeast1-b",
		// MachineType: "projects/youtubedownload-259206/zones/asia-northeast1-b/machineTypes/g1-small",
		MachineType: "projects/youtubedownload-259206/zones/asia-northeast1-b/machineTypes/f1-micro",
		DisplayDevice: &compute.DisplayDevice{
			EnableDisplay: false,
		},
		Metadata: &compute.Metadata{
			Kind: "compute#metadata",
			Items: []*compute.MetadataItems{
				&compute.MetadataItems{
					Key:   "gce-container-declaration",
					Value: &gceContainerDeclarationValue,
				},
				&compute.MetadataItems{
					Key:   "google-logging-enabled",
					Value: &googleLogginEnabledValue,
				},
			},
		},
		Tags: &compute.Tags{
			Items: []string{
				"http-server",
			},
		},
		Disks: []*compute.AttachedDisk{
			&compute.AttachedDisk{
				Kind:       "compute#attachedDisk",
				Type:       "PERSISTENT",
				Boot:       true,
				Mode:       "READ_WRITE",
				AutoDelete: true,
				DeviceName: "youtube-api",
				InitializeParams: &compute.AttachedDiskInitializeParams{
					SourceImage: "projects/cos-cloud/global/images/cos-stable-78-12499-59-0",
					DiskType:    "projects/youtubedownload-259206/zones/asia-northeast1-b/diskTypes/pd-standard",
					DiskSizeGb:  10,
				},
				DiskEncryptionKey: &compute.CustomerEncryptionKey{},
			},
		},
		CanIpForward: false,
		NetworkInterfaces: []*compute.NetworkInterface{
			&compute.NetworkInterface{
				Kind:       "compute#networkInterface",
				Subnetwork: "projects/youtubedownload-259206/regions/asia-northeast1/subnetworks/default",
				AccessConfigs: []*compute.AccessConfig{
					&compute.AccessConfig{
						Kind:        "compute#accessConfig",
						Name:        "External NAT",
						Type:        "ONE_TO_ONE_NAT",
						NetworkTier: "PREMIUM",
						NatIP:       "34.84.133.238",
					},
				},
				AliasIpRanges: []*compute.AliasIpRange{},
			},
		},
		Description: "",
		Labels: map[string]string{
			"container-vm": "cos-stable-78-12499-59-0",
		},
		Scheduling: &compute.Scheduling{
			Preemptible:       false,
			OnHostMaintenance: "MIGRATE",
			AutomaticRestart:  &trueBool,
			NodeAffinities:    []*compute.SchedulingNodeAffinity{},
		},
		DeletionProtection: false,
		ReservationAffinity: &compute.ReservationAffinity{
			ConsumeReservationType: "ANY_RESERVATION",
		},
		ServiceAccounts: []*compute.ServiceAccount{
			&compute.ServiceAccount{
				Email: "44266997063-compute@developer.gserviceaccount.com",
				Scopes: []string{
					"https://www.googleapis.com/auth/devstorage.read_only",
					"https://www.googleapis.com/auth/logging.write",
					"https://www.googleapis.com/auth/monitoring.write",
					"https://www.googleapis.com/auth/servicecontrol",
					"https://www.googleapis.com/auth/service.management.readonly",
					"https://www.googleapis.com/auth/trace.append",
				},
			},
		},
	}

	_, err := instanceService.Insert(projectID, zone, &instance).Do()

	if err != nil {
		log.Println("err: ", err)
		return
	}

	log.Println("Success")

}

// deleteInstance インスタンスを削除する
func deleteInstance(service *compute.Service) {
	instanceService := service.Instances
	instanceService.Delete(projectID, zone, "youtube-api").Do()
}

func getClient(ctx context.Context) (*http.Client, error) {
	return google.DefaultClient(ctx)
}

func getComputeService(client *http.Client) (*compute.Service, error) {
	return compute.New(client)
}

// func config() *oauth2.Config {
// 	return &oauth2.Config{
// 		ClientID:     "44266997063-9sp0lops1sv08l2fml3rm8e8jl99pgi9.apps.googleusercontent.com",
// 		ClientSecret: "YsfOJEuZgEo58ReLZpn55xo9",
// 		Endpoint:     google.Endpoint,
// 		Scopes:       []string{urlshortener.UrlshortenerScope},
// 	}
// }

// getContext contextを取得する
func getContext() context.Context {
	// return context.WithValue(
	// 	context.Background(),
	// 	oauth2.HTTPClient,
	// 	&http.Client{
	// 		Transport: &transport.APIKey{Key: developerKey}
	// 	}
	// )

	return context.Background()
}
